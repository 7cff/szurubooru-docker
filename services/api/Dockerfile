FROM python:3.5-jessie

LABEL maintainer="tehtotalpwnage@gmail.com"

##
# Normally I manage package dependencies using Alpine Linux, but there's yet to be any uncomplicated
# means of installing python, pip, and it's entire bloody family. Therefore, larger yet stable Debian.
#
# RUN apk add --no-cache ffmpeg git libgcc libgfortran libstdc++ musl nodejs postgresql-dev
RUN echo 'deb http://deb.debian.org/debian jessie-backports main' >> /etc/apt/sources.list
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt install -y ffmpeg nodejs

# Allow us to not run the scripts as root.
#RUN mkdir -p /var/www/html/config
RUN mkdir -p /var/www/html
RUN addgroup --gid 9999 szurubooru
# RUN addgroup -g 9999 szurubooru
RUN adduser --disabled-login --ingroup szurubooru --gecos '' --uid 9999 szurubooru
# RUN adduser -D -G szurubooru -g '' -u 9999 szurubooru
RUN chown -R szurubooru:szurubooru /var/www/html
USER szurubooru

# Fetch dependencies.
RUN git clone https://github.com/rr-/szurubooru.git /var/www/html/szurubooru

# Set up initial configuration file.
# As much as I want to turn this into a container that can be pushed onto Quay.io,
# this is literally impossible to do. Unlike a Laravel project, running this program
# depends on the immediate state of the config file, and there aren't any real solutions
# to setting up a config file. Until configs become available in what I'm using
# (Rancher), this is the best solution.
# Oh, and by this I mean I'm shoving it at the end of the file so it doesn't rebuild
# each time I change the config.yaml just a slight bit.
# COPY config.yaml /var/www/html/config/config.yaml
# RUN ln -s /var/www/html/config/config.yaml /var/www/html/szurubooru/config.yaml

# Initialize client dependencies.
WORKDIR /var/www/html/szurubooru/client
RUN npm install

# Initialize server dependencies.
# YES, I KNOW I should normally use a virtualenv. But one, it's already containerized,
# so who cares. TWO, virtualenv here would mean an even larger image.
WORKDIR /var/www/html/szurubooru/server
USER root
RUN pip3 install --no-cache-dir -r requirements.txt
RUN pip3 install --no-cache-dir waitress

# We're going to use a wrapper script to run szurubooru due to the need for migrations
# and client dependencies to be built. No, I'm not building them as part of the
# image build process. The ultimate goal is that when configs are implemented in
# Rancher that I can just implement a mounted config solution and have this script
# already in place.
COPY start.sh /usr/local/bin/start
RUN chmod +x /usr/local/bin/start

USER szurubooru

# In  the past, I'd copy the config.yaml over to the container, and this would only
# work in local and private registry environments. Instead, I'm leaving the mounting
# of the config file to literally anyone else.
# COPY config.yaml /var/www/html/szurubooru/config.yaml

WORKDIR /var/www/html/szurubooru/server

ENTRYPOINT ["/usr/local/bin/start"]
