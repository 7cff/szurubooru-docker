#!/bin/bash

##
# This is a wrapper script to aid in properly running szurubooru in a Docker container.
# Before starting the API service, this application will attempt to build the client
# dependencies and run any pending migrations.
##

echo "Waiting 15 seconds for Postgres to be ready."
sleep 15
echo "Running database migrations."
cd /var/www/html/szurubooru/server
alembic upgrade head

echo "Building client dependencies."
cd /var/www/html/szurubooru/client
npm run build

echo "Starting API service."
cd /var/www/html/szurubooru/server
waitress-serve --port 6666 szurubooru.facade:app
